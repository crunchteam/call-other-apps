package net.crunchdroid.android.recipes;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickHandler(View view) {

        Intent intent;
        switch (view.getId()) {

            case R.id.btn_show_map:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:21.422527, 39.826353"));
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    showMessage("Activity Not Found");
                }
                break;
            case R.id.btn_show_google_play:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.tmkeen.alquran"));
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    showMessage("Activity Not Found");
                }
                break;
            case R.id.btn_show_email:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setData(Uri.parse("mailto:"));
                intent.setType("message/rfc822");
                String to[] = {"email1@email.com", "email2@email.com"};
                String cc[] = {"email3@email.com", "email4@email.com"};
                String bcc[] = {"email5@email.com", "email6@email.com"};

                intent.putExtra(Intent.EXTRA_EMAIL, to);
                intent.putExtra(Intent.EXTRA_CC, cc);
                intent.putExtra(Intent.EXTRA_BCC, bcc);
                intent.putExtra(Intent.EXTRA_SUBJECT, "My subject .....");
                intent.putExtra(Intent.EXTRA_TEXT, "My message .....");
                try {
                    startActivity(Intent.createChooser(intent, "Email"));
                } catch (ActivityNotFoundException e) {
                    showMessage("Activity Not Found");
                }

                break;
            case R.id.btn_show_sms:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("smsto:"));
                intent.setType("vnd.android-dir/mms-sms");
                String phoneNumber = "0102030405; 0106070809";
                intent.putExtra("address", phoneNumber);
                intent.putExtra("sms_body", "My message ......");

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    showMessage("Activity Not Found");
                }

                break;
            case R.id.btn_show_call:
                intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:0102030405"));

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    showMessage("Activity Not Found");
                }

                break;
            case R.id.btn_show_dial:
                intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:0102030405"));

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    showMessage("Activity Not Found");
                }

                break;
            case R.id.btn_show_clock:
                intent = getPackageManager().getLaunchIntentForPackage("com.sec.android.app.clockpackage");

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    showMessage("Activity Not Found");
                }

                break;

        }


    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
